#******************************************************************************
#  @file  RunRF.py
#  @brief Launch and Parse results generated by regression framework.
#
#  ---------------------------------------------------------------------------
#******************************************************************************/
# Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

#!/usr/bin/env python
import subprocess
import io
import pandas as pd
import numpy as np
import argparse

def showAllTest():
    result = subprocess.run(["adb","shell","regressionframework -m"],stdout=subprocess.PIPE,)
    data = io.StringIO(result.stdout.decode('utf-8'))
    df = pd.read_csv(data, sep="|")
    df_array = np.array(df)
    print(df.to_string(index=False))

# Construct the argument parser
ap = argparse.ArgumentParser()

# Add the arguments to the parser
ap.add_argument("-n", "--name", required=False,
   help="to Run test with Test Name, Use -l option to list available tests")
ap.add_argument("-u", "--uid", required=False,type=str,
   help="to Run test with UID, Use -l option to list available tests")
ap.add_argument("-i", "--iteration", required=False,type=int,default=1,
   help="Number of iterations to run test.")
ap.add_argument("-l", "--list",action='store_true',default=False, required=False, help="List all test registered in regressionframework")
ap.add_argument("-t", "--testtype", required=False,
   help="To Run test with Type, Use -l option to list available tests")
ap.add_argument("-m", "--module", required=False,
   help="To Run test where module, Use -l option to list available tests")

args = vars(ap.parse_args())
result = subprocess.run(["adb","shell","regressionframework -m"],stdout=subprocess.PIPE,)
data = io.StringIO(result.stdout.decode('utf-8'))

testName = args['name']

if (args['list']):
    showAllTest()
    exit()
print(args)



df = pd.read_csv(data, sep="|")
df_array = np.array(df)
df_dict = df.to_dict()
print (df_dict)
n = len(df_dict['UID'])
print(n)
run_test = []
numIterations = args['iteration']

for i in  range (0,n):
    if (testName in df_dict['TESTNAME'][i]):
        run_test.append(df_dict['UID'][i])

print(run_test)
print(numIterations)
