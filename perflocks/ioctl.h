/******************************************************************************
  @file    ioctl.h
  @brief   Definitions of ioctl calls made to display driver.

  DESCRIPTION

  ---------------------------------------------------------------------------
******************************************************************************/
// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

int early_wakeup_ioctl(int connectorId);
