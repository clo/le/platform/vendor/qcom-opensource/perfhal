/******************************************************************************
  @file    OptsHandlerExtnStub.cpp
  @brief   Implementation of performance server module extn empty stubs

  DESCRIPTION

  ---------------------------------------------------------------------------
******************************************************************************/
// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "OptsHandlerExtn.h"

int32_t init_pasr() {
    return FAILED;
}

int32_t pasr_entry_func(Resource &r, OptsData &d) {
    return SUCCESS;
}

int32_t pasr_exit_func(Resource &r, OptsData &d) {
    return SUCCESS;
}


