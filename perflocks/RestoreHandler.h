/******************************************************************************
  @file    RestoreHandler.h
  @brief   Implementation of nodes restoration

  DESCRIPTION

  ---------------------------------------------------------------------------
******************************************************************************/
// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef __RESET_HANDLER__H_
#define __RESET_HANDLER__H_

#include "ResourceInfo.h"
#include "OptsData.h"
#include "Target.h"

class ResetHandler {
    private:
        static ResetHandler mResetHandler;

    private:
        ResetHandler();
        ResetHandler(ResetHandler const& rh);
        ResetHandler& operator=(ResetHandler const& rh);

    public:
        ~ResetHandler();

        int8_t Init();

        static void reset_to_default_values(OptsData &d);
        static void reset_freq_to_default(OptsData &d);
        static void reset_sched_boost(OptsData &d);
        static void reset_cpu_nodes(int8_t cpu);

        static ResetHandler &getInstance() {
            return mResetHandler;
        }

};

#endif /*__RESET_HANDLER__H_*/
