/******************************************************************************
  @file  VendorIGlue.cpp
  @brief  VendorIGlue glues modules to the framework which needs perf events

  ---------------------------------------------------------------------------
******************************************************************************/
// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "PerfGlueLayer.h"
#include "VendorIGlue.h"

VendorIGlue::VendorIGlue(const char *libname, int32_t *events, int32_t numevents) {
    PerfGlueLayer(libname, events, numevents);
}

VendorIGlue::~VendorIGlue() {
}
