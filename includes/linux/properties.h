// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef __PROPERTIES_H__
#define __PROPERTIES_H__

#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif

#define PROPERTY_VALUE_MAX 92

static inline int property_set(const char *key, const char *value)
{
    return 0;
}
static inline int property_get(const char* key, char* value, const char* default_value)
{
    return 0;
}
static inline int8_t property_get_bool(const char *key, int8_t default_value)
{
    return 0;
}

#ifdef __cplusplus
}
#endif

#endif // __PROPERTIES_H__
