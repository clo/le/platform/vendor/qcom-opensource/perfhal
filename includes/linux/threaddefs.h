// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef __THREADDEFS_H__
#define __THREADDEFS_H__

enum{
  ANDROID_PRIORITY_HIGHEST        = -20,
};
namespace android{
enum{
    PRIORITY_HIGHEST        = ANDROID_PRIORITY_HIGHEST,
};
}
#endif // __THREADDEFS_H__
