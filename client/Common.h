/******************************************************************************
  @file    Common.h
  @brief   All the common headers are included here

  DESCRIPTION

  ---------------------------------------------------------------------------
 ******************************************************************************/
// Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef __COMMON_H99__
#define __COMMON_H99__

//include common headers here

void getthread_name(char *buf,int buf_size);

#endif /*__COMMON_H99__*/
